import { ChangeDetectorRef, Component } from '@angular/core';
import { Firestore, collection, collectionData, doc, docData, addDoc, deleteDoc, updateDoc } from '@angular/fire/firestore';
import { NavigationExtras, Router } from '@angular/router';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { GoogleCloudVisionService } from '../google-cloud-vision.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  segment = 0;
  obj: any;
  found = [];
  notFound = [];
  imageData = '';
  toast: HTMLIonToastElement;

  constructor(
    private route: Router,
    private camera: Camera,
    private firestore: Firestore,
    private cd: ChangeDetectorRef,
    public alertController: AlertController,
    private vision: GoogleCloudVisionService,
    public loadingController: LoadingController,
    public toastController: ToastController,
  ) {
    // load all treasures
    this.getNotFound();
    // load  found treasures
    this.getFound();
  }

  segmentChanged(e: any) {
    this.segment = parseInt(e.detail.value, 10);
  }

  getNotFound() {
    this.getNf().subscribe(res => {
      this.notFound = res
      this.cd.detectChanges();
    });
  }

  getFound() {
    this.getF().subscribe(res => {
      this.found = res
      this.cd.detectChanges();
    });
  }

  async updateObject(obj: any) {
    await this.deleteNf(obj);
    await this.addF(obj);
  }


  // calls to firebase------------------------------------------------
  getNf() {
    const collectionRef = collection(this.firestore, 'treasure');
    return collectionData(collectionRef, { idField: 'id' });
  }

  getF() {
    const collectionRef = collection(this.firestore, 'found');
    return collectionData(collectionRef, { idField: 'id' });
  }

  deleteNf(treasure: any) {
    const docRef = doc(this.firestore, `treasure/${treasure.id}`);
    return deleteDoc(docRef);
  }

  addF(treasure: any) {
    const collectionRef = collection(this.firestore, 'found');
    return addDoc(collectionRef, treasure);
  }




  async takePhoto(obj: any) {
    const options: CameraOptions = {
      quality: 100,
      targetHeight: 500,
      targetWidth: 500,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      // correctOrientation: true
    }

    this.camera.getPicture(options).then(async (imageData) => {
      const loading = await this.loadingController.create({
        message: 'Checking Results...',
        translucent: true
      });
      await loading.present();
      this.vision.getLabels(imageData).subscribe(async (result: any) => {
        let labels = this.extractLabels(result.responses[0].labelAnnotations as any[]);
        await loading.dismiss()
        if (this.checkResult(labels, obj.name)) {
          this.updateObject(obj);
          this.presentToast('Congratulations Treasure found !!', 'success');
        }
        else {
          this.presentToast('Sorry wrong object !!', 'danger');
        }
      }
        , err => {
        });
    }, err => {

    });



  }

  async selectPhoto(obj: any) {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
    }

    this.camera.getPicture(options).then(async (imageData) => {
      this.imageData = imageData;
      const loading = await this.loadingController.create({
        message: 'Getting Results...',
        translucent: true
      });

      await loading.present();
      this.vision.getLabels(imageData).subscribe(async (result: any) => {
        let labels = this.extractLabels(result.responses[0].labelAnnotations as any[]);
        await loading.dismiss()
        if (this.checkResult(labels, obj.name)) {
          this.presentToast('Congratulations Treasure found !!', 'success');
          this.updateObject(obj);
        }
        else {
          this.presentToast('Sorry wrong object !!', 'danger');
        }
      }, err => {

      });
    }, (err) => {

    })
  }

  async checkObject(obj: any) {
    const alert = await this.alertController.create({
      header: 'Select one option ',
      message: 'Take Photo or Select from Galary!',
      buttons: [
        {
          text: 'Camera',
          role: 'camera',
          handler: () => {
            this.takePhoto(obj);
          }
        }, {
          text: 'Gallary',
          role: 'gallary',
          handler: () => {
            this.selectPhoto(obj);
          }
        }
      ]
    });
    await alert.present();
  }

  extractLabels(res: any[]) {
    let labels = [];
    for (const item of res) {
      let lbs = item.description.split(' ');
      labels.push(
        ...lbs
      )
    }
    return labels;
  }

  checkResult(labels: string[], name: string) {
    name = name.split(' ').filter(s => s).join('')
    name = name.toLocaleLowerCase();
    console.log(name);
    console.log(labels);

    for (const label of labels) {
      if (
        label.toLocaleLowerCase().includes(name) || name.includes(label.toLocaleLowerCase())
      ) {
        return true
      }
    }
    return false;
  }


  async presentToast(txt, color) {
    try {
      this.toast.dismiss();
    }
    catch (e) { }
    this.toast = await this.toastController.create({
      message: txt,
      duration: 2500,
      color: color || 'success',
      position: 'top'
    });
    this.toast.present();
  }



}
